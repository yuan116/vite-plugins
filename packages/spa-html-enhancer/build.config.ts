import { defineBuildConfig } from 'unbuild';

import rollupConfig from '../../shared/unbuild.rollup.config';

export default defineBuildConfig({
    clean: true,
    declaration: true,
    entries: ['./src/index'],
    externals: ['web-app-manifest'],
    rollup: rollupConfig,
});
