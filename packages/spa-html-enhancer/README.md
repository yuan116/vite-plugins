# Vite Plugin - SPA HTML Enhancer

## Installation

```bash
pnpm add -D gitlab:yuan116/vite-plugins#path:packages/spa-html-enhancer
```

## Usage

```ts
// vite config file
import spaIndexHTML from '@yuan116/vite-plugin-spa-html-enhancer';
...

export default {
    // vite config
    ...
    plugins: [spaIndexHTML(parameters)]
}
```

## Parameters

| Property       | Type                                                                        | Default Value            | Optional |
| -------------- | --------------------------------------------------------------------------- | ------------------------ | -------- |
| `icons`        | [`Icon[]`](./src/index.ts#L107)                                             | `[]`                     | &check;  |
| `manifest`     | `string`                                                                    | '`manifest.webmanifest`' | &check;  |
| `manifestData` | [`@types/web-app-manifest.WebAppManifest`](#additional-docs) or `undefined` | `undefined`              | &check;  |
| `metas`        | [`Meta[]`](./src/index.ts#L112)                                             | `[]`                     | &check;  |
| `policy`       | [`ContentSecurityPolicy[]`](./src/index.ts#L6) or `undefined`               | `undefined`              | &check;  |
| `src`          | `string`                                                                    | '`index.html`'           | &check;  |
| `title`        | `string`                                                                    | ''                       | &check;  |

## Types

## Additional Docs

1. [@types/web-app-manifest](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/web-app-manifest/index.d.ts)
