import { Plugin } from 'vite';
import { WebAppManifest } from 'web-app-manifest';

interface ContentSecurityPolicy {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/base-uri */
    'base-uri'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/child-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'child-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/connect-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'connect-src'?: Partial<PolicyCommonDirectives>;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/default-src */
    'default-src'?: Partial<PolicyAllDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/font-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'font-src'?: Partial<PolicyCommonDirectives>;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/form-action */
    'form-action'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/frame-src
     *
     * If missing, will fallback to 'child-src' directive, then 'default-src' directive
     */
    'frame-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/img-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'img-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/manifest-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'manifest-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/media-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'media-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/object-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'object-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'script-src'?: Partial<PolicyCommonDirectives & PolicyStrictDynamic & PolicyUnsafeDirectives & PolicyWasmUnsafeEval>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src-attr
     *
     * If missing, will fallback to 'script-src' directive, then 'default-src' directive
     */
    'script-src-attr'?: ContentSecurityPolicy['script-src'];
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src-elem
     *
     * If missing, will fallback to 'script-src' directive, then 'default-src' directive
     */
    'script-src-elem'?: ContentSecurityPolicy['script-src'];
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'style-src'?: Partial<PolicyCommonDirectives & PolicyUnsafeDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src-attr
     *
     * If missing, will fallback to 'style-src' directive, then 'default-src' directive
     */
    'style-src-attr'?: ContentSecurityPolicy['style-src'];
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src-elem
     *
     * If missing, will fallback to 'style-src' directive, then 'default-src' directive
     */
    'style-src-elem'?: ContentSecurityPolicy['style-src'];
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/upgrade-insecure-requests */
    'upgrade-insecure-requests'?: boolean;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/worker-src
     *
     * If missing, will fallback to 'child-src' directive, then 'script-src' directive, then 'default-src' directive
     */
    'worker-src'?: ContentSecurityPolicy['script-src'];
}
type Icon = {
    /**
     * Sets or retrieves a destination URL or an anchor point.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/HTMLLinkElement/href)
     */
    href: string;
    /**
     * Sets or retrieves the relationship between the object and the destination of the link.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/HTMLLinkElement/rel)
     */
    rel: string;
    /** '32x32', '180x180', '512x512', etc... */
    sizes: string;
    /**
     * Sets or retrieves the MIME type of the object.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/HTMLLinkElement/type)
     */
    type: string;
};
type Meta = {
    content: string;
    name: string;
};
interface PolicyCommonDirectives {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#host-source */
    hosts?: string[];
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#none */
    none?: boolean;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#self */
    self?: boolean;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#scheme-source */
    scheme?: {
        blob?: boolean;
        data?: boolean;
        filesystem?: boolean;
        http?: boolean;
        https?: boolean;
        mediastream?: boolean;
    };
}
interface PolicyStrictDynamic {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#strict-dynamic */
    'strict-dynamic'?: boolean;
}
interface PolicyUnsafeDirectives {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#unsafe-eval */
    'unsafe-eval'?: boolean;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#unsafe-inline */
    'unsafe-inline'?: boolean;
}
interface PolicyWasmUnsafeEval {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#wasm-unsafe-eval */
    'wasm-unsafe-eval'?: boolean;
}
interface PolicyAllDirectives extends PolicyCommonDirectives, PolicyStrictDynamic, PolicyUnsafeDirectives, PolicyWasmUnsafeEval {
}
interface SpaHtmlEnhancerPluginConfig {
    icons: Icon[];
    /** @default 'manifest.webmanifest' */
    manifest: string;
    manifestData: WebAppManifest | undefined;
    metas: Meta[];
    policy: ContentSecurityPolicy | undefined;
    /** @default 'index.html' */
    src: string;
    /** @default '' */
    title: string;
}
declare function spaHtmlEnhancer(pluginConfig?: Partial<SpaHtmlEnhancerPluginConfig>): Plugin[];

export { type ContentSecurityPolicy, type Icon, type Meta, type PolicyAllDirectives, type PolicyCommonDirectives, type PolicyStrictDynamic, type PolicyUnsafeDirectives, type PolicyWasmUnsafeEval, type SpaHtmlEnhancerPluginConfig, spaHtmlEnhancer as default };
