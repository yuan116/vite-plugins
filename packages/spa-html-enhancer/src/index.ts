import type { HtmlTagDescriptor, Plugin, ResolvedConfig } from 'vite';
import type { WebAppManifest } from 'web-app-manifest';

import { generateFileURL, resolvePath, toRelativeRootURL } from '@yuan116/vite-plugin-utilities';
import fsExtra from 'fs-extra';

export interface ContentSecurityPolicy {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/base-uri */
    'base-uri'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/child-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'child-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/connect-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'connect-src'?: Partial<PolicyCommonDirectives>;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/default-src */
    'default-src'?: Partial<PolicyAllDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/font-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'font-src'?: Partial<PolicyCommonDirectives>;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/form-action */
    'form-action'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/frame-src
     *
     * If missing, will fallback to 'child-src' directive, then 'default-src' directive
     */
    'frame-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/img-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'img-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/manifest-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'manifest-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/media-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'media-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/object-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'object-src'?: Partial<PolicyCommonDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'script-src'?: Partial<PolicyCommonDirectives & PolicyStrictDynamic & PolicyUnsafeDirectives & PolicyWasmUnsafeEval>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src-attr
     *
     * If missing, will fallback to 'script-src' directive, then 'default-src' directive
     */
    'script-src-attr'?: ContentSecurityPolicy['script-src'];
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src-elem
     *
     * If missing, will fallback to 'script-src' directive, then 'default-src' directive
     */
    'script-src-elem'?: ContentSecurityPolicy['script-src'];
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src
     *
     * If missing, will fallback to 'default-src' directive
     */
    'style-src'?: Partial<PolicyCommonDirectives & PolicyUnsafeDirectives>;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src-attr
     *
     * If missing, will fallback to 'style-src' directive, then 'default-src' directive
     */
    'style-src-attr'?: ContentSecurityPolicy['style-src'];
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src-elem
     *
     * If missing, will fallback to 'style-src' directive, then 'default-src' directive
     */
    'style-src-elem'?: ContentSecurityPolicy['style-src'];
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/upgrade-insecure-requests */
    'upgrade-insecure-requests'?: boolean;
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/worker-src
     *
     * If missing, will fallback to 'child-src' directive, then 'script-src' directive, then 'default-src' directive
     */
    'worker-src'?: ContentSecurityPolicy['script-src'];
}

export type Icon = {
    /**
     * Sets or retrieves a destination URL or an anchor point.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/HTMLLinkElement/href)
     */
    href: string;
    /**
     * Sets or retrieves the relationship between the object and the destination of the link.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/HTMLLinkElement/rel)
     */
    rel: string;
    /** '32x32', '180x180', '512x512', etc... */
    sizes: string;
    /**
     * Sets or retrieves the MIME type of the object.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/HTMLLinkElement/type)
     */
    type: string;
};

export type Meta = {
    content: string;
    name: string;
};

export interface PolicyCommonDirectives {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#host-source */
    hosts?: string[];
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#none */
    none?: boolean;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#self */
    self?: boolean;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#scheme-source */
    scheme?: {
        blob?: boolean;
        data?: boolean;
        filesystem?: boolean;
        http?: boolean;
        https?: boolean;
        mediastream?: boolean;
    };
}

export interface PolicyStrictDynamic {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#strict-dynamic */
    'strict-dynamic'?: boolean;
}

export interface PolicyUnsafeDirectives {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#unsafe-eval */
    'unsafe-eval'?: boolean;
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#unsafe-inline */
    'unsafe-inline'?: boolean;
}

export interface PolicyWasmUnsafeEval {
    /** @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources#wasm-unsafe-eval */
    'wasm-unsafe-eval'?: boolean;
}

export interface PolicyAllDirectives extends PolicyCommonDirectives, PolicyStrictDynamic, PolicyUnsafeDirectives, PolicyWasmUnsafeEval {}

export interface SpaHtmlEnhancerPluginConfig {
    icons: Icon[];
    /** @default 'manifest.webmanifest' */
    manifest: string;
    manifestData: WebAppManifest | undefined;
    metas: Meta[];
    policy: ContentSecurityPolicy | undefined;
    /** @default 'index.html' */
    src: string;
    /** @default '' */
    title: string;
}

export default function spaHtmlEnhancer(pluginConfig?: Partial<SpaHtmlEnhancerPluginConfig>): Plugin[] {
    const resolvedPluginConfig: SpaHtmlEnhancerPluginConfig = {
        icons: pluginConfig?.icons ?? [],
        manifest: pluginConfig?.manifest?.trim() ?? 'manifest.webmanifest',
        manifestData: pluginConfig?.manifestData,
        metas: pluginConfig?.metas ?? [],
        policy: pluginConfig?.policy,
        src: pluginConfig?.src?.trim() || 'index.html',
        title: pluginConfig?.title ?? '',
    };

    let viteConfig: ResolvedConfig;
    let htmlPath = '';
    let htmlContent = '';
    const hasManifest = resolvedPluginConfig.manifestData !== undefined;

    return [
        {
            config(userConfig, env) {
                return {
                    appType: 'spa',
                };
            },
            configResolved(config) {
                viteConfig = config;
                htmlPath = resolvePath(viteConfig.root, resolvedPluginConfig.src);
                htmlContent = fsExtra.readFileSync(htmlPath, 'utf8');

                if (hasManifest) {
                    if (Array.isArray(resolvedPluginConfig.manifestData?.icons)) {
                        resolvedPluginConfig.manifestData.icons = resolvedPluginConfig.manifestData.icons.map((icon) => {
                            const src = generateFileURL(viteConfig, icon.src);

                            return { ...icon, src };
                        });
                    }
                    resolvedPluginConfig.manifestData!.start_url = viteConfig.base;
                }
            },
            configureServer(server) {
                return () =>
                    server.middlewares.use(async (request, response, next) => {
                        if (hasManifest && request?.originalUrl?.endsWith(resolvedPluginConfig.manifest)) {
                            response.end(JSON.stringify(resolvedPluginConfig.manifestData));
                        } else if (request.url === '/index.html') {
                            response.end(await server.transformIndexHtml(request.url, htmlContent, request.originalUrl));
                        }

                        next();
                    });
            },
            enforce: 'pre',
            handleHotUpdate(context) {
                if (context.file === htmlPath) {
                    Promise.resolve(context.read()).then((content) => {
                        htmlContent = content;
                        context.server.ws.send({
                            path: '*',
                            triggeredBy: htmlPath,
                            type: 'full-reload',
                        });
                    });
                }
            },
            load(id) {
                if (isRootIndexHtml(viteConfig, id)) {
                    return htmlContent;
                }

                return;
            },
            name: 'vite-plugin-spa-html-enhancer-pre',
            resolveId(source, importer, options) {
                if (isRootIndexHtml(viteConfig, source)) {
                    return viteConfig.isProduction ? source : htmlPath;
                }

                return;
            },
            transformIndexHtml: {
                handler(html, context) {
                    const tags: HtmlTagDescriptor[] = [];

                    if (resolvedPluginConfig.policy !== undefined) {
                        const directives = [
                            'base-uri',
                            'child-src',
                            'connect-src',
                            'default-src',
                            'font-src',
                            'form-action',
                            'frame-src',
                            'img-src',
                            'manifest-src',
                            'media-src',
                            'object-src',
                            'script-src',
                            'script-src-attr',
                            'script-src-elem',
                            'style-src',
                            'style-src-attr',
                            'style-src-elem',
                            'worker-src',
                        ] as const;
                        const policies = [];

                        for (const directive of directives) {
                            if (resolvedPluginConfig.policy[directive] !== undefined) {
                                const values = resolvePolicy(directive, resolvedPluginConfig.policy[directive]);

                                if (values.length > 0) {
                                    policies.push(directive + ' ' + values.join(' ') + ';');
                                }
                            }
                        }

                        if (resolvedPluginConfig.policy['upgrade-insecure-requests']) {
                            policies.push('upgrade-insecure-requests;');
                        }

                        if (policies.length > 0) {
                            tags.push({
                                attrs: {
                                    content: policies.join(' '),
                                    'http-equiv': 'Content-Security-Policy',
                                },
                                injectTo: 'head',
                                tag: 'meta',
                            });
                        }
                    }

                    for (const icon of resolvedPluginConfig.icons) {
                        icon.href = toRelativeRootURL(icon.href);

                        tags.push({
                            attrs: icon,
                            injectTo: 'head',
                            tag: 'link',
                        });
                    }

                    for (const meta of resolvedPluginConfig.metas) {
                        tags.push({
                            attrs: meta,
                            injectTo: 'head',
                            tag: 'meta',
                        });
                    }

                    if (hasManifest) {
                        tags.push({
                            attrs: {
                                href: generateFileURL(viteConfig, resolvedPluginConfig.manifest),
                                rel: 'manifest',
                            },
                            injectTo: 'head',
                            tag: 'link',
                        });
                    }

                    if (resolvedPluginConfig.title !== '') {
                        tags.push({
                            children: resolvedPluginConfig.title,
                            injectTo: 'head',
                            tag: 'title',
                        });
                    }

                    return {
                        html: htmlContent,
                        tags,
                    };
                },
                order: 'pre',
            },
        },
        {
            apply: 'build',
            enforce: 'post',
            generateBundle(options, bundle, isWrite) {
                if (hasManifest) {
                    this.emitFile({
                        fileName: resolvedPluginConfig.manifest,
                        source: JSON.stringify(resolvedPluginConfig.manifestData),
                        type: 'asset',
                    });
                }
            },
            name: 'vite-plugin-spa-index-html-post',
            transformIndexHtml: {
                handler(html, context) {
                    return html
                        .replaceAll(/<!--(.*?)-->/g, '')
                        .replaceAll('\n', '')
                        .replaceAll(/[\t ]+</g, '<')
                        .replaceAll(/>[\t ]+</g, '><')
                        .replaceAll(/>[\t ]+$/g, '>')
                        .replaceAll(/[\t\n ]+/g, ' ')
                        .replaceAll(/>\s+/g, '>');
                },
                order: 'post',
            },
        },
    ];
}

function isRootIndexHtml(viteConfig: ResolvedConfig, id: string) {
    return id === resolvePath(viteConfig.root, 'index.html');
}

const schemes = ['blob', 'data', 'filesystem', 'http', 'https', 'mediastream'] as const;
const allowStrictDynamicDirectiveSet = new Set(['default-src', 'script-src', 'script-src-attr', 'script-src-elem', 'worker-src']);
const allowUnsafeDirectiveSet = new Set([...allowStrictDynamicDirectiveSet, 'style-src', 'style-src-attr', 'style-src-elem']);
const allowWasmUnsafeEvelDirectiveSet = allowStrictDynamicDirectiveSet;

function resolvePolicy(directive: keyof Omit<ContentSecurityPolicy, 'upgrade-insecure-requests'>, policy: PolicyAllDirectives) {
    const values = [];

    if (policy.none) {
        values.push(`'none'`);
    }

    if (policy.self) {
        values.push(`'self'`);
    }

    if (Array.isArray(policy.hosts)) {
        values.push(...policy.hosts.filter(Boolean));
    }

    for (const scheme of schemes) {
        if (policy.scheme?.[scheme]) {
            values.push(scheme + ':');
        }
    }

    // if (allowStrictDynamicDirectiveSet.has(directive) && policy['strict-dynamic']) {
    //     values.push(`'strict-dynamic'`);
    // }

    if (allowUnsafeDirectiveSet.has(directive) && policy['unsafe-inline']) {
        values.push(`'unsafe-inline'`);
    }

    if (allowUnsafeDirectiveSet.has(directive) && policy['unsafe-eval']) {
        values.push(`'unsafe-eval'`);
    }

    if (allowWasmUnsafeEvelDirectiveSet.has(directive) && policy['wasm-unsafe-eval']) {
        values.push(`'wasm-unsafe-eval'`);
    }

    return values;
}
