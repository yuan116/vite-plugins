import type { Plugin, Rollup } from 'vite';

import { resolveNodeModule, resolvePath } from '@yuan116/vite-plugin-utilities';
import fsExtra from 'fs-extra';
import path from 'node:path';
import { isCSSRequest } from 'vite';

export interface SplitChunkPluginConfig {
    /** @default false */
    byNamespace: boolean;
    /** @default false */
    nodeModulesOnly: boolean;
    /** @default false */
    override: boolean;
    /** @default '' */
    prefix: string;
}

export default function splitChunk(pluginConfig?: Partial<SplitChunkPluginConfig>): Plugin {
    const resolvedPluginConfig: SplitChunkPluginConfig = {
        byNamespace: pluginConfig?.byNamespace ?? false,
        nodeModulesOnly: pluginConfig?.nodeModulesOnly ?? false,
        override: pluginConfig?.override ?? false,
        prefix: pluginConfig?.prefix ?? '',
    };

    return {
        apply: 'build',
        config(userConfig, env) {
            const output = userConfig?.build?.rollupOptions?.output;

            if (output === undefined) {
                return {
                    build: {
                        rollupOptions: {
                            output: {
                                manualChunks(id, meta) {
                                    return getChunkName(id, meta, resolvedPluginConfig);
                                },
                            },
                        },
                    },
                };
            }

            const outputs = Array.isArray(output) ? output : [output];

            for (const output of outputs) {
                output.manualChunks = createSplitChunk(resolvedPluginConfig, output);
            }
        },
        name: 'vite-plugin-split-chunk',
    };
}

function createSplitChunk(pluginConfig: SplitChunkPluginConfig, output: Rollup.OutputOptions): Rollup.GetManualChunk {
    const previousManualChunks = output.manualChunks;

    return (id, meta) => {
        if (!pluginConfig.override && typeof previousManualChunks === 'function') {
            const chunkName = previousManualChunks(id, meta);

            if (typeof chunkName === 'string') {
                return chunkName;
            }
        }

        return getChunkName(id, meta, pluginConfig);
    };
}

const nodeModulePackageNameMap = new Map<string, false | string>();

function getChunkName(id: string, meta: Rollup.ManualChunkMeta, pluginConfig: SplitChunkPluginConfig) {
    const moduleInfo = meta.getModuleInfo(id)!;

    if (moduleInfo.isEntry) {
        return;
    }

    if (moduleInfo.isIncluded) {
        const nodeModulesIndex = id.lastIndexOf('node_modules');
        if (nodeModulesIndex !== -1) {
            const nodeModuleNames = id.substring(nodeModulesIndex).split(path.sep).slice(1);

            if (pluginConfig.byNamespace) {
                return 'vendor-' + nodeModuleNames[0];
            }

            const nodeModulePath = id.substring(0, nodeModulesIndex) + 'node_modules';

            const searchNodeModuleNames = [];
            for (const nodeModuleName of nodeModuleNames) {
                searchNodeModuleNames.push(nodeModuleName);

                const mapKey = searchNodeModuleNames.join('.');

                if (!nodeModulePackageNameMap.has(mapKey)) {
                    try {
                        const packageName =
                            'vendor-' +
                            fsExtra
                                .readJSONSync(resolveNodeModule(resolvePath(nodeModulePath, searchNodeModuleNames.join(path.sep), 'package.json')), 'utf8')
                                .name.replaceAll('/', '-');

                        nodeModulePackageNameMap.set(mapKey, packageName);
                        return packageName;
                    } catch {
                        nodeModulePackageNameMap.set(mapKey, false);
                        continue;
                    }
                }

                const packageName = nodeModulePackageNameMap.get(mapKey);

                if (typeof packageName === 'string') {
                    return packageName;
                }
            }
        } else if (moduleInfo.id.startsWith('\u0000')) {
            // vite/preload-helper.js
            // commonjsHelpers.js
            return 'vendor-vite';
        } else if (!pluginConfig.nodeModulesOnly) {
            const pathInfo = path.parse(id);

            if (isCSSRequest(id)) {
                return pluginConfig.prefix + pathInfo.name;
            }

            return pluginConfig.prefix + pathInfo.dir.split(path.sep).at(-1);
        }
    }

    return;
}
