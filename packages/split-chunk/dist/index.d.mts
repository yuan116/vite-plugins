import { Plugin } from 'vite';

interface SplitChunkPluginConfig {
    /** @default false */
    byNamespace: boolean;
    /** @default false */
    nodeModulesOnly: boolean;
    /** @default false */
    override: boolean;
    /** @default '' */
    prefix: string;
}
declare function splitChunk(pluginConfig?: Partial<SplitChunkPluginConfig>): Plugin;

export { type SplitChunkPluginConfig, splitChunk as default };
