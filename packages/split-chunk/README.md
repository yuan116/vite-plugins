# Vite Plugin - Split Chunk

## Installation

```bash
pnpm add -D gitlab:yuan116/vite-plugins#path:packages/split-chunk
```

## Usage

```ts
// vite config file
import splitChunk from '@yuan116/vite-plugin-split-chunk';
...

export default {
    // vite config
    ...
    plugins: [splitChunk(parameters)]
}
```

## Parameters

| Property          | Type      | Default Value | Optional |
| ----------------- | --------- | ------------- | -------- |
| `byNamespace`     | `boolean` | `false`       | &check;  |
| `nodeModulesOnly` | `boolean` | `false`       | &check;  |
| `override`        | `boolean` | `false`       | &check;  |
| `prefix`          | `string`  | ''            | &check;  |
