import { Plugin } from 'vite';
import { InjectManifestOptions } from 'workbox-build';

interface ServiceWorkerPluginConfig {
    /** @default false */
    devEnabled: boolean;
    /** @default 'service-worker.js' */
    fileName: string;
    injectManifestOptions: Omit<InjectManifestOptions, 'globDirectory' | 'swDest' | 'swSrc'>;
    /** @default true */
    precache: boolean;
    /** @default false */
    precacheFonts: boolean;
    /** @default false */
    precacheImages: boolean;
    /** @default false */
    precacheJson: boolean;
    /** @default '' */
    srcDir: string;
}
declare function serviceWorker(pluginConfig: Partial<ServiceWorkerPluginConfig>): Plugin;

export { type ServiceWorkerPluginConfig, serviceWorker as default };
