# Service Worker

## Workbox Event:

| Event       | Description                                                 | Remark                                         |
| ----------- | ----------------------------------------------------------- | ---------------------------------------------- |
| installing  | service worker installing                                   |                                                |
| installed   | service worker installed                                    |                                                |
| waiting     | service worker installed, awaiting confirmation             | use skipWaiting to update service worker       |
| activating  | service worker is updating                                  |                                                |
| controlling | service worker updated, page is using previous version      | may reload page, for avoid serving wrong cache |
| activated   | service worker updated, page is using latest service worker |                                                |
| redundant   | service worker updated, page using outdated service worker  | required immediate page reload                 |
| message     | service worker response message                             |

## Reference Docs

-   Register and handle workbox service worker  
    https://developer.chrome.com/docs/workbox/handling-service-worker-updates/  
    https://developer.chrome.com/docs/workbox/modules/workbox-window/#the-very-first-time-a-service-worker-is-installed

-   Service worker cache, route  
    https://developer.chrome.com/docs/workbox/faster-multipage-applications-with-streams/#streaming-responses  
    https://developer.chrome.com/docs/workbox/modules/workbox-routing/  
    https://developer.chrome.com/docs/workbox/using-plugins/  
    https://developer.chrome.com/docs/workbox/modules/workbox-strategies/
