# Vite Plugin - Service Worker

## Installation

```bash
pnpm add -D gitlab:yuan116/vite-plugins#path:packages/service-worker
```

## Usage

```ts
// vite config file
import serviceWorker from '@yuan116/vite-plugin-service-worker';
...

export default {
    // vite config
    ...
    plugins: [serviceWorker(parameters)]
}
```

## Parameters

| Property                | Type                                  | Default Value          | Optional |
| ----------------------- | ------------------------------------- | ---------------------- | -------- |
| `devEnabled`            | `boolean`                             | `false`                | &check;  |
| `fileName`              | `string`                              | '`service-worker.js`'  | &check;  |
| `injectManifestOptions` | `workbox-build.InjectManifestOptions` | '`self.__WB_MANIFEST`' | &check;  |
| `precacheFonts`         | `boolean`                             | `false`                | &check;  |
| `precacheImages`        | `boolean`                             | `false`                | &check;  |
| `precacheJson`          | `boolean`                             | `false`                | &check;  |
| `srcDir`                | `string`                              | ''                     | &check;  |

## Additional Docs

1. [Workbox Build](https://developer.chrome.com/docs/workbox/reference/workbox-build)
