import type { InlineConfig, Plugin, ResolvedConfig } from 'vite';
import type { InjectManifestOptions } from 'workbox-build';

import { resolveOutputDirectory, resolvePath } from '@yuan116/vite-plugin-utilities';
import fsExtra from 'fs-extra';
import path from 'node:path';
import { build } from 'vite';
import { injectManifest } from 'workbox-build';

export interface ServiceWorkerPluginConfig {
    /** @default false */
    devEnabled: boolean;
    /** @default 'service-worker.js' */
    fileName: string;
    injectManifestOptions: Omit<InjectManifestOptions, 'globDirectory' | 'swDest' | 'swSrc'>;
    /** @default true */
    precache: boolean;
    /** @default false */
    precacheFonts: boolean;
    /** @default false */
    precacheImages: boolean;
    /** @default false */
    precacheJson: boolean;
    /** @default '' */
    srcDir: string;
}

const PLUGIN_ID = 'vite-plugin-service-worker';

export default function serviceWorker(pluginConfig: Partial<ServiceWorkerPluginConfig>): Plugin {
    const resolvedPluginConfig: ServiceWorkerPluginConfig = {
        devEnabled: pluginConfig?.devEnabled ?? false,
        fileName: pluginConfig?.fileName || 'service-worker.js',
        injectManifestOptions: {
            injectionPoint: 'self.__WB_MANIFEST',
            ...pluginConfig?.injectManifestOptions,
        },
        precache: pluginConfig?.precache ?? true,
        precacheFonts: pluginConfig?.precacheFonts ?? false,
        precacheImages: pluginConfig?.precacheImages ?? false,
        precacheJson: pluginConfig?.precacheJson ?? false,
        srcDir: pluginConfig?.srcDir ?? '',
    };

    let viteConfig: ResolvedConfig;
    let serviceWorkerSrc = '';

    return {
        async closeBundle() {
            const { fileName, injectManifestOptions, precache, precacheFonts, precacheImages, precacheJson } = resolvedPluginConfig;
            const serviceWorkerOutputDir = resolveOutputDirectory(viteConfig);

            const buildConfig = {
                base: viteConfig.base,
                build: {
                    commonjsOptions: viteConfig.build.commonjsOptions,
                    copyPublicDir: false,
                    emptyOutDir: false,
                    outDir: serviceWorkerOutputDir,
                    rollupOptions: {
                        input: serviceWorkerSrc,
                        output: {
                            compact: true,
                            entryFileNames: fileName,
                            generatedCode: {
                                constBindings: true,
                                objectShorthand: true,
                            },
                            indent: false,
                            minifyInternalExports: true,
                        },
                        strictDeprecations: true,
                    },
                    sourcemap: false,
                },
                configFile: false,
                define: {
                    ...viteConfig.define,
                    'process.env.NODE_ENV': JSON.stringify(viteConfig.mode),
                },
                esbuild: viteConfig.esbuild,
                publicDir: false,
                resolve: viteConfig.resolve,
                root: viteConfig.root,
            } satisfies InlineConfig;

            await build(buildConfig);

            if (hasWorkboxManifestInjectionPoint(injectManifestOptions)) {
                const precacheExtensions = [];
                if (precache) {
                    if (!Array.isArray(injectManifestOptions.globPatterns)) {
                        injectManifestOptions.globPatterns = [];
                    }

                    precacheExtensions.push('html', 'css', 'js');

                    if (precacheFonts) {
                        precacheExtensions.push('ttf', 'woff', 'woff2');
                    }
                    if (precacheImages) {
                        precacheExtensions.push('gif', 'jpeg', 'jpg', 'ico', 'png', 'svg', 'webp');
                    }
                    if (precacheJson) {
                        precacheExtensions.push('json');
                    }

                    injectManifestOptions.globPatterns.push('manifest.webmanifest', `**/*.{${precacheExtensions.join(',')}}`);
                }

                const serviceWorkerPath = path.join(serviceWorkerOutputDir, fileName);

                await injectManifest({
                    ...injectManifestOptions,
                    globDirectory: buildConfig.build.outDir,
                    swDest: serviceWorkerPath,
                    swSrc: serviceWorkerPath,
                });
            }
        },
        configResolved(config) {
            viteConfig = config;

            serviceWorkerSrc = resolvePath(viteConfig.root, resolvedPluginConfig.srcDir, resolvedPluginConfig.fileName);
        },
        enforce: 'post',
        load(id) {
            if (id === PLUGIN_ID) {
                if (resolvedPluginConfig.devEnabled) {
                    const content = fsExtra
                        .readFileSync(serviceWorkerSrc, 'utf8')
                        .replaceAll('./', path.join(viteConfig.base, resolvedPluginConfig.srcDir, '/').replaceAll(path.sep, '/'));

                    if (hasWorkboxManifestInjectionPoint(resolvedPluginConfig.injectManifestOptions)) {
                        return content.replaceAll(resolvedPluginConfig.injectManifestOptions.injectionPoint!, '[]');
                    }

                    return content;
                }

                return '';
            }

            return;
        },
        name: PLUGIN_ID,
        resolveId(source, importer, options) {
            if (source.endsWith(resolvedPluginConfig.fileName)) {
                return PLUGIN_ID;
            }

            return;
        },
    };
}

function hasWorkboxManifestInjectionPoint(injectManifestOptions: ServiceWorkerPluginConfig['injectManifestOptions']) {
    return typeof injectManifestOptions.injectionPoint === 'string' && injectManifestOptions.injectionPoint !== '';
}
