import { Plugin } from 'vite';

interface CesiumPluginConfig {
    /** @default 'cesium' */
    baseURL: string;
    /** @default true */
    injectCSS: boolean;
    /** @default true */
    injectJS: boolean;
    /** @default false */
    minify: boolean;
}
declare function cesium(pluginConfig?: Partial<CesiumPluginConfig>): Plugin;

export { type CesiumPluginConfig, cesium as default };
