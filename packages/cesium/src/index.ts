import type { EsbuildTransformOptions, HtmlTagDescriptor, Plugin, ResolvedConfig, UserConfig } from 'vite';

import { checkPluginDependencies, resolveOutputDirectory } from '@yuan116/vite-plugin-utilities';
import fsExtra from 'fs-extra';
import path from 'node:path';
import sirv from 'sirv';
import { transformWithEsbuild } from 'vite';

export interface CesiumPluginConfig {
    /** @default 'cesium' */
    baseURL: string;
    /** @default true */
    injectCSS: boolean;
    /** @default true */
    injectJS: boolean;
    /** @default false */
    minify: boolean;
}

export default function cesium(pluginConfig?: Partial<CesiumPluginConfig>): Plugin {
    checkPluginDependencies('vite-plugin-cesium', [{ name: 'cesium', url: 'https://www.npmjs.com/package/cesium' }]);

    const resolvedPluginConfig: CesiumPluginConfig = {
        baseURL: pluginConfig?.baseURL ?? 'cesium',
        injectCSS: pluginConfig?.injectCSS ?? true,
        injectJS: pluginConfig?.injectJS ?? true,
        minify: pluginConfig?.minify ?? false,
    };

    let viteConfig: ResolvedConfig;
    const CESIUM_BUILD_PATH = 'node_modules/cesium/Build/';
    let baseURL = '';

    return {
        async closeBundle() {
            const paths = ['Assets', 'ThirdParty', 'Widgets/Images', 'Widgets/InfoBox', resolvedPluginConfig.injectJS ? 'Cesium.js' : 'Workers'];

            if (resolvedPluginConfig.injectCSS) {
                paths.push('Widgets/widgets.css');
            }

            const cesiumOutputDirectory = resolveOutputDirectory(viteConfig, resolvedPluginConfig.baseURL);

            for (const _path of paths) {
                fsExtra.copySync(path.join(CESIUM_BUILD_PATH, 'Cesium', _path), path.join(cesiumOutputDirectory, _path));
            }

            if (resolvedPluginConfig.minify) {
                const esbuildOptions = {
                    css: {
                        legalComments: 'none',
                        minify: false,
                        minifyIdentifiers: false,
                        minifySyntax: true,
                        minifyWhitespace: true,
                    } satisfies EsbuildTransformOptions,
                    js: {
                        legalComments: 'none',
                        minify: false,
                        minifyIdentifiers: false,
                        minifySyntax: false,
                        minifyWhitespace: true,
                    } satisfies EsbuildTransformOptions,
                } as const;

                for (const file of fsExtra
                    .readdirSync(cesiumOutputDirectory, {
                        encoding: 'utf8',
                        recursive: true,
                    })
                    .filter((file) => file.endsWith('.css') || file.endsWith('.js'))) {
                    const filePath = path.join(cesiumOutputDirectory, file);
                    const extension = file.split('.').at(-1)! as keyof typeof esbuildOptions;

                    const result = await transformWithEsbuild(fsExtra.readFileSync(filePath, 'utf8'), file, esbuildOptions[extension]);
                    fsExtra.writeFileSync(filePath, result.code, 'utf8');
                }
            }
        },
        config(userConfig, env) {
            baseURL = path.join(userConfig.base ?? '/', resolvedPluginConfig.baseURL, '/').replaceAll(path.sep, '/');

            const config: UserConfig = {
                define: {
                    CESIUM_BASE_URL: JSON.stringify(baseURL),
                },
            };

            if (resolvedPluginConfig.injectJS) {
                config.build = {
                    rollupOptions: {
                        external: ['cesium'],
                    },
                };
            }

            return config;
        },
        configResolved(config) {
            viteConfig = config;
        },
        configureServer(server) {
            server.middlewares.use(
                baseURL,
                sirv(path.join(CESIUM_BUILD_PATH, 'CesiumUnminified'), {
                    extensions: [],
                }),
            );
        },
        name: 'vite-plugin-cesium',
        transformIndexHtml(html, context) {
            const tags: HtmlTagDescriptor[] = [];

            if (resolvedPluginConfig.injectJS) {
                tags.push({
                    attrs: {
                        src: baseURL + 'Cesium.js',
                    },
                    injectTo: 'head',
                    tag: 'script',
                });
            }

            if (resolvedPluginConfig.injectCSS) {
                tags.push({
                    attrs: {
                        href: baseURL + 'Widgets/widgets.css',
                        rel: 'stylesheet',
                    },
                    injectTo: 'head',
                    tag: 'link',
                });
            }

            return tags;
        },
    };
}
