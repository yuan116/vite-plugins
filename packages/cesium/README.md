# Vite Plugin - Cesium

## Required Node Dependencies

1. `cesium`

## Installation

```bash
pnpm add -D gitlab:yuan116/vite-plugins#path:packages/cesium
```

## Usage

```ts
// vite config file
import cesium from '@yuan116/vite-plugin-cesium';
...

export default {
    // vite config
    ...
    plugins: [cesium(parameters)]
}
```

## Parameters

| Property    | Type      | Default Value | Optional |
| ----------- | --------- | ------------- | -------- |
| `baseURL`   | `string`  | '`cesium`'    | &check;  |
| `injectCSS` | `boolean` | `true`        | &check;  |
| `injectJS`  | `boolean` | `true`        | &check;  |
| `minify`    | `boolean` | `false`       | &check;  |
