import { ResolvedConfig } from 'vite';

interface Dependency {
    name: string;
    url: string;
}
declare function checkPluginDependecies(pluginName: string, dependencies: Dependency[]): void;

declare function titleColor(string: string): string;
declare function keywordColor(string: string, quote?: boolean): string;

declare function generateFileURL(viteConfig: ResolvedConfig, file: string): string;

declare function getExecutable(executableName: string): string | undefined;

interface LogOptions {
    /** @default 0 */
    spaces?: number;
    title?: string;
}
interface ErrorLogOptions extends LogOptions {
    exit?: boolean;
}
declare const _default$1: {
    error(message: string, logOptions?: ErrorLogOptions): void;
    info(message: string, logOptions?: LogOptions): void;
    warn(message: string, logOptions?: LogOptions): void;
};

declare function registerExitHandler(callback: () => void): void;

declare const _default: NodeJS.RequireResolve;

declare function resolveOutputDirectory(viteConfig: ResolvedConfig, ...subOutputDirectories: string[]): string;

declare function resolvePath(_path: string, ...subPaths: string[]): string;

declare function toRelativeRootURL(url: string): string;

export { type Dependency, checkPluginDependecies as checkPluginDependencies, generateFileURL, getExecutable, keywordColor, _default$1 as logger, registerExitHandler, _default as resolveNodeModule, resolveOutputDirectory, resolvePath, titleColor, toRelativeRootURL };
