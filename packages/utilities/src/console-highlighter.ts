import chalk from 'chalk';

export function titleColor(string: string) {
    return chalk.cyan.bold(string);
}

export function keywordColor(string: string, quote = false) {
    string = chalk.yellowBright(string);

    if (quote) {
        return '"' + string + '"';
    }

    return string;
}
