const callbackSet = new Set<() => void>();

export default function registerExitHandler(callback: () => void) {
    callbackSet.add(callback);
    listenExitEvent();
}

let exitHandlersBound = false;

function listenExitEvent() {
    if (!exitHandlersBound) {
        process.on('exit', (code) => {
            for (const callback of callbackSet) {
                callback();
            }

            process.exit(code ?? 0);
        });
        process.on('SIGINT', () => process.exit());
        process.on('SIGTERM', () => process.exit());
        process.on('SIGHUP', () => process.exit());

        exitHandlersBound = true;
    }
}
