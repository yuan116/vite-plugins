import chalk from 'chalk';

import { titleColor } from './console-highlighter';

export interface LogOptions {
    /** @default 0 */
    spaces?: number;
    title?: string;
}

export interface ErrorLogOptions extends LogOptions {
    exit?: boolean;
}

const ERROR = chalk.bgRed.bold(' ERROR ');
const WARN = chalk.bgYellowBright.bold(' WARN ');
const INFO = chalk.bgCyanBright.bold(' WARN ');

export default {
    error(message: string, logOptions?: ErrorLogOptions) {
        console.log(makeMessage(ERROR, message, logOptions!));

        if (logOptions?.exit) {
            process.exit(1);
        }
    },
    info(message: string, logOptions?: LogOptions) {
        console.log(makeMessage(INFO, message, logOptions!));
    },
    warn(message: string, logOptions?: LogOptions) {
        console.log(makeMessage(WARN, message, logOptions!));
    },
};

function makeMessage(level: string, message: string, { spaces = 0, title }: LogOptions) {
    const messages = [level, message];

    if (typeof title === 'string') {
        messages.splice(1, 0, titleColor(title));
    }

    return messages.join(' ').padStart(spaces, ' ');
}
