import path from 'node:path';

import resolveNodeModule from './resolve-node-module';

export default function getExecutable(executableName: string) {
    try {
        return resolveNodeModule(path.join('.bin', executableName));
    } catch {
        // eslint-disable-next-line unicorn/no-useless-undefined
        return undefined;
    }
}
