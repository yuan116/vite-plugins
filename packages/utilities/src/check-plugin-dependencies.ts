import { keywordColor } from './console-highlighter';
import logger from './logger';
import resolveNodeModule from './resolve-node-module';

export interface Dependency {
    name: string;
    url: string;
}

export default function checkPluginDependecies(pluginName: string, dependencies: Dependency[]) {
    const missingDependencies = dependencies.filter((dependency) => !check(dependency.name));

    if (missingDependencies.length > 0) {
        logger.error('missing dependencies:', { title: pluginName });
        console.log(
            missingDependencies
                .map((dependency) => ` - [?](?)`.replace('?', keywordColor(dependency.name)).replace('?', keywordColor(dependency.url)))
                .join('\n'),
        );
        console.log();

        process.exit(1);
    }
}

function check(dependencyName: string) {
    try {
        resolveNodeModule(dependencyName);

        return true;
    } catch {
        return false;
    }
}
