export default function toRelativeRootURL(url: string) {
    return '/' + url.split('/').filter(Boolean).join('/');
}
