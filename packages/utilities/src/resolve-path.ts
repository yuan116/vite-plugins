import path from 'node:path';

export default function resolvePath(_path: string, ...subPaths: string[]) {
    return path.resolve(_path, path.join(...subPaths));
}
