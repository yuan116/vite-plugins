import type { ResolvedConfig } from 'vite';

import path from 'node:path';

export default function generateFileURL(viteConfig: ResolvedConfig, file: string) {
    if (viteConfig.isProduction) {
        return path.join(viteConfig.base, file);
    }

    return file;
}
