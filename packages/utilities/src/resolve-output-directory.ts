import type { ResolvedConfig } from 'vite';

import resolvePath from './resolve-path';

export default function resolveOutputDirectory(viteConfig: ResolvedConfig, ...subOutputDirectories: string[]) {
    return resolvePath(viteConfig.root, viteConfig.build.outDir, ...subOutputDirectories);
}
