import module from 'node:module';

export default module.createRequire(import.meta.url).resolve;
