import type { Plugin, ResolvedConfig } from 'vite';

import { checkPluginDependencies, getExecutable, logger, registerExitHandler, resolveOutputDirectory, resolvePath } from '@yuan116/vite-plugin-utilities';
import chalk from 'chalk';
import fsExtra from 'fs-extra';
import childProcess from 'node:child_process';

export interface CapacitorLiveReloadPluginConfig {
    /** @default 'android' */
    androidPath: string;
    /** @default true */
    autoCopy: boolean;
    /** @default 'capacitor-live-reload-url' */
    filename: string;
    /** @default 'ios' */
    iosPath: string;
}

let liveReloadFile = '';

export default function capacitorLiveReload(pluginConfig?: Partial<CapacitorLiveReloadPluginConfig>): Plugin {
    checkPluginDependencies('Capacitor Live Reload', [
        { name: '@capacitor/cli', url: 'https://www.npmjs.com/package/@capacitor/cli' },
        { name: '@capacitor/core', url: 'https://www.npmjs.com/package/@capacitor/core' },
    ]);

    const resolvedPluginConfig: CapacitorLiveReloadPluginConfig = {
        androidPath: pluginConfig?.androidPath ?? 'android',
        autoCopy: pluginConfig?.autoCopy ?? true,
        filename: pluginConfig?.filename?.trim() || 'capacitor-live-reload-url',
        iosPath: pluginConfig?.iosPath ?? 'ios',
    };

    let viteConfig: ResolvedConfig;

    return {
        apply: 'serve',
        config(userConfig, env) {
            if (userConfig?.server?.host === undefined) {
                return {
                    server: {
                        host: true,
                    },
                };
            }
        },
        configResolved(config) {
            viteConfig = config;

            const outputDirectory = resolveOutputDirectory(viteConfig);
            fsExtra.mkdirpSync(outputDirectory);
            liveReloadFile = resolvePath(outputDirectory, resolvedPluginConfig.filename);

            removeLiveReloadFile();
        },
        configureServer(server) {
            server.watcher.unwatch([resolvedPluginConfig.androidPath + '/**', resolvedPluginConfig.iosPath + '/**']);

            server.httpServer!.on('listening', () => {
                setTimeout(() => {
                    if (server.resolvedUrls !== null) {
                        const localIP = server.resolvedUrls.network.find((networkURL) => !new URL(networkURL).hostname.startsWith('100.64.0'));
                        console.log();

                        if (localIP === undefined) {
                            logger.warn('Capacitor Live Reload not available', { spaces: 2 });
                            console.log('         Please ensure your device is connected to a network');
                            console.log();

                            return;
                        }

                        fsExtra.writeFileSync(liveReloadFile, localIP, 'utf8');

                        if (resolvedPluginConfig.autoCopy) {
                            const capacitorExecutable = getExecutable('capacitor');

                            if (capacitorExecutable !== undefined) {
                                try {
                                    childProcess.execFileSync(capacitorExecutable, ['copy'], { encoding: 'utf8' });

                                    logger.info('capacitor.config.json copied', { spaces: 2 });
                                } catch {
                                    //
                                }
                            }
                        }

                        console.log(
                            '  ? ?: ?'.replace('?', chalk.green('➜')).replace('?', chalk.bold('Capacitor Live Reload')).replace('?', chalk.green(localIP)),
                        );

                        registerExitHandler(removeLiveReloadFile);
                    }
                }, 100);
            });
        },
        name: 'vite-plugin-capacitor-live-reload',
    };
}

function removeLiveReloadFile() {
    if (fsExtra.existsSync(liveReloadFile)) {
        fsExtra.rmSync(liveReloadFile);
    }
}
