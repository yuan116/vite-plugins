import { Plugin } from 'vite';

interface CapacitorLiveReloadPluginConfig {
    /** @default 'android' */
    androidPath: string;
    /** @default true */
    autoCopy: boolean;
    /** @default 'capacitor-live-reload-url' */
    filename: string;
    /** @default 'ios' */
    iosPath: string;
}
declare function capacitorLiveReload(pluginConfig?: Partial<CapacitorLiveReloadPluginConfig>): Plugin;

export { type CapacitorLiveReloadPluginConfig, capacitorLiveReload as default };
