# Vite Plugin - Capacitor Live Reload

## Required Node Dependencies

1. `@capacitor/android`
2. `@capacitor/core`
3. `@capacitor/ios`

## Required Node Dev Dependencies

1. `@capacitor/cli`

## Installation

```bash
pnpm add -D gitlab:yuan116/vite-plugins#path:packages/capacitor-live-reload
```

## Usage

```ts
// vite config file
import capacitorLiveReload from '@yuan116/vite-plugin-capacitor-live-reload';
...

export default {
    // vite config
    ...
    plugins: [capacitorLiveReload(parameters)]
}
```

2. Copy below code to `capacitor.config.ts`

```ts
// capacitor.config.ts
import fsExtra from 'fs-extra';
import path from 'node:path';
...

const config = {
    // capacitor config
    ...
};

const file = path.normalize(path.join(config.webDir ?? 'www', 'capacitor-live-reload-url'));
if (fsExtra.existsSync(file)) {
    const url = fsExtra.readFileSync(file, 'utf-8');

    if (url !== null) {
        if (typeof config.server === 'undefined') {
            config.server = {};
        }

        config.server.cleartext = true;
        config.server.url = url;
    }
}

export default config;
```

## Parameters

| Property      | Type      | Default Value                 | Optional |
| ------------- | --------- | ----------------------------- | -------- |
| `androidPath` | `string`  | '`android`'                   | &check;  |
| `autoCopy`    | `boolean` | `true`                        | &check;  |
| `filename`    | `string`  | '`capacitor-live-reload-url`' | &check;  |
| `iosPath`     | `string`  | '`ios`'                       | &check;  |
