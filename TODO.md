1. [] - Enhance `capacitor-live-reload`
    - [] remove `filename` props
    - [] new utility function `getLiveReloadURL`
2. [] - Enhance `cesium` package
    - [] Show warning when using both `injectCSS` and import css
    - [] Show warning when using both `injectJS` and import script
3. [] - Enhance `service-worker` package
    - [] Modify result code of importing service worker path
    - [] Show info of `recommend using workbox-* dependencies to dev` when `workbox-window` is not installed
4. [] - Enhance `spa-html-enhancer` package
    - [] Support `unsafe-hashes` `nonce-<base64-value>` `<hash-algorithm>-<base64-value>`, refer [Vite Plugin CSP Guard](https://github.com/RockiRider/csp)
    - [] Show warning when using `unsafe-*`
