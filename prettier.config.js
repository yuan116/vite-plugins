import prettier from '@yuan116/config-files/prettier';

export default prettier.jsDoc().sortImports().config;
