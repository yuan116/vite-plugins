**Prerequisites:**

1. [Git](https://git-scm.com/)
2. [pnpm 9](https://pnpm.io/)
3. [Node JS 22](https://pnpm.io/cli/env)
4. `Others`
    1. [VS Code Extension](https://code.visualstudio.com/) (optional but recommend to install)
        1. [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
        2. [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

## Plugin Docs

1. [Capacitor Live Reload](./packages/capacitor-live-reload/README.md)
2. [Cesium](./packages/cesium/README.md)
3. [Service Worker](./packages/service-worker/README.md)
4. [SPA Index HTML](./packages/spa-index-html/README.md)
5. [Split Chunk](./packages/split-chunk/README.md)

## IMPORTANT NOTE

Before using any plugin above, copy below content and paste to `.pnpmfile.cjs`

```js
// .pnpmfile.cjs
function readPackage(pkg, context) {
    if (pkg.name.startsWith('@yuan116/')) {
        if (pkg.dependencies['@yuan116/vite-plugin-utilities'] !== undefined) {
            pkg.dependencies['@yuan116/vite-plugin-utilities'] = 'gitlab:yuan116/vite-plugins#path:packages/utilities';
        }
    }

    return pkg;
}

module.exports = {
    hooks: {
        readPackage,
    },
};
```
