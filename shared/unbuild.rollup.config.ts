import type { BuildConfig } from 'unbuild';

export default {
    cjsBridge: true,
    dts: {
        compilerOptions: {
            composite: false,
        },
    },
    emitCJS: true,
    esbuild: {
        minifyIdentifiers: true,
        minifySyntax: true,
        minifyWhitespace: true,
        treeShaking: true,
    },
    inlineDependencies: true,
    output: {
        compact: true,
        generatedCode: {
            constBindings: true,
            objectShorthand: true,
        },
        indent: false,
        minifyInternalExports: true,
    },
} satisfies BuildConfig['rollup'];
