import eslint from '@yuan116/config-files/eslint';

export default eslint
    .shared(import.meta.dirname)
    .ignores('packages/**/dist/')
    .jsConfig('*.config.js')
    .node('packages/**/src/*.ts').configs;
